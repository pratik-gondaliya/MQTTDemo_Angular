import { Component } from '@angular/core';
import {Paho} from 'ng2-mqtt/mqttws31';
import { MQTTService } from './services/mqtt.service';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {  
  title = 'app works!';
  client:Paho.MQTT.Client;
  public txtMessage:string;

  public env;
  public messageList = [];
  constructor(public mqttService:MQTTService) {
    this.env = environment;
    console.log('connect mqtt client called');
    this.mqttService.connectClient().subscribe(data=>{
        this.onConnected();  
    });
  }

  onConnected() {
    console.log('client successfully connected');
    this.mqttService.subscribeTopic('sample').subscribe(msg=>{
      this.messageList.push(msg);
    });
    
  }
  
  sendMessage(message: string) {
    let packet = new Paho.MQTT.Message(message);
    packet.destinationName = "sample";
    this.client.send(packet);
  }

  onMessage() {
    this.client.onMessageArrived = (message: Paho.MQTT.Message) => {
      this.messageList.push(message.payloadString);
      console.log('Message arrived : ' + message.payloadString);
    };
  }

  onConnectionLost() {
    this.client.onConnectionLost = (responseObject: Object) => {
      console.log('Connection lost : ' + JSON.stringify(responseObject));
    };
  }

  public onsendMessage() {
      this.mqttService.sendMessage('sample',this.txtMessage);
      this.txtMessage = '';
  }
}
