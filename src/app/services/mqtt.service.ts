import { Injectable } from '@angular/core';
import { Paho } from 'ng2-mqtt/mqttws31';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MQTTService {
  private _conncetionSubject = new Subject<any>();
  private _messageSubject = new Subject<any>();
  private _client:Paho.MQTT.Client;
  
  constructor() { 
    let number = Math.floor(Math.random() * (1000000 - 1 + 1)) + 1;
    this._client = new Paho.MQTT.Client(environment.mqttHost, environment.mqttPort, 'qwerty'+number);
    
    this._client.onMessageArrived = (message: Paho.MQTT.Message) => {
      this._messageSubject.next(message.payloadString);
    };
  }

  public sendMessage(topic:string,message: string) {
    let packet = new Paho.MQTT.Message(message);
    packet.destinationName = topic;
    packet.retained = true;
    this._client.send(packet);
  }

  public connectClient() {
    this._client.connect({onSuccess: this.onConnected.bind(this),userName:'pratik',password:'vt@123'});
    return this._conncetionSubject.asObservable();
  }

  private onConnected() {
    console.log("Connected");
    this._conncetionSubject.next(1);
  }
  public subscribeTopic(topic:string):Observable<any> {
    this._client.subscribe(topic,undefined);
    return this._messageSubject.asObservable();
  }

  public unSubscribeTopic(topic:string) {
    this._client.unsubscribe(topic,undefined);
  }
}
