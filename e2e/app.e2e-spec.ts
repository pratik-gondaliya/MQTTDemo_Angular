import { MQTTDemoPage } from './app.po';

describe('mqttdemo App', () => {
  let page: MQTTDemoPage;

  beforeEach(() => {
    page = new MQTTDemoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
